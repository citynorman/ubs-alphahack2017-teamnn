#******************************************************************
#******************************************************************
#******************************************************************
# sample strategy backtesting and submission template
#******************************************************************
#******************************************************************
#******************************************************************
import platform
import math
import pandas as pd
import numpy as np
import scipy.stats

from sklearn import linear_model
#import lightgbm as lgb

import seaborn as sns
import matplotlib.pyplot as plt

#******************************************************************
# qs
#******************************************************************
'''
-run all analysis on ID_TICKER_PIT? or ID_SEC_TS?
-US_Models-Relative_Value_Model.csv => ranks? all 0-100

'''

#******************************************************************
# cfg
#******************************************************************

#-------------------------------
# files
#-------------------------------
cfg_fname_sm = '../data/secmaster/Russell1KSecMaster-with-RP.pkl'
cfg_fname_sm_csv = '../data/secmaster/Russell1KSecMaster-with-RP.csv'
cfg_fname_ihs_factors = '../data/ihs-mth/df_data-raw-ihs-US_Factors-mth.csv'
cfg_fname_ihs_model_rv = '../data/ihs-mth/df_data-raw-ihs-US_Models-Relative_Value_Model-mth.csv'
cfg_fname_rp_sentiment = '../data/ravenpack-mth/RavenPackAnalytics_Companies_1.0_2011-tqa-r1k-mth.pkl'
cfg_fname_rp_sentiment_csv = '../data/ravenpack-mth/RavenPackAnalytics_Companies_1.0_2011-tqa-r1k-mth.csv'
cfg_fname_qdl_sidr = '../data/quandl/DNB_SIDR_b9d46928a1b81371d2cb2aa97a605b5b.csv'

#-------------------------------
# columns
#-------------------------------
cfg_col_sm_cols_drop = [
    'DATE_YYYY_MM_MAX', 'DATE_IS_SUBMIT', 'ID_SEC','GICS_SECT_PIT_ID','GICS_INDU_PIT_ID',
       'ID_TICKER_CUR', 'ID_CUSIP_PIT', 'ID_SEDOL_CUR', 'ID_CUSIP_CUR', 
       'GICS_SECT_PIT_ID', 'GICS_INDU_PIT_ID', 'GICS_SUBIND_PIT_ID', 
       'IDX_WEIGHT',
]

cfg_col_ihs_cols_drop = [
    'cusip', 'name','ID_TICKER_PIT'
]

#-------------------------------
# ihs
#-------------------------------
cfg_col_ihs_alpha_factors = [
'ActRtn12M',
'TW_EBITDAEV',
'TW_SP',
'TWBP',
'TWCP',
'FC_Y2REpsG',
'FC_Y3SUR',
'FCFEquity',
'FCFROI',
#'FC_PDY',
'FC_CVFY1EPS',
'FC_DYPEG',
'FC_EGP',
'FC_EPSRM',
'FC_FCFP',
'FC_FwdROE',
'FC_HLEP'
]
cfg_col_ihs_alpha_factors = [s.lower() for s in cfg_col_ihs_alpha_factors]


cfg_col_ihs_alpha_model_rv = [
    'fmrelval', 'rvm_capacq',
       'rvm_composite', 'rvm_estep', 'rvm_roe', 'rvm_ttmfcfp'
]

cfg_col_ihs_alpha = cfg_col_ihs_alpha_factors

cfg_col_ihs_alpha_q = [s+'_q' for s in cfg_col_ihs_alpha]
cfg_col_ihs_alpha_zs_cur = [s+'_zs_cur' for s in cfg_col_ihs_alpha]


#-------------------------------
# Ravenpack
#-------------------------------
cfg_col_rp_alpha = [
    'EVENT_SENTIMENT_SCORE_wgt'
]

#-------------------------------
# Quandl
#-------------------------------
cfg_col_qdl_alpha = [
    'sidr'
]


#-------------------------------
# model
#-------------------------------
cfg_col_alpha = cfg_col_ihs_alpha#+cfg_col_rp_alpha+cfg_col_qdl_alpha

cfg_col_alpha_high_better=cfg_col_ihs_alpha
cfg_col_X = cfg_col_ihs_alpha
cfg_col_X_train = cfg_col_ihs_alpha_zs_cur

cfg_col_y_rtn = 'RTN_FWD_1M'

#-------------------------------
# trading
#-------------------------------
cfg_col_model = ['model_ols_reg']
cfg_col_model_pred = [s+'_pred' for s in cfg_col_model]
cfg_col_model_q = [s+'_q' for s in cfg_col_model_pred]

cfg_col_trade_q = cfg_col_model_q+[s+'_q' for s in ['fc_fcfp']]
cfg_col_trade_rtn = ['port_rtn_%s' %s for s in cfg_col_trade_q]

cfg_tiles_features = 10
cfg_tiles_trade = cfg_tiles_features
cfg_fld_indu = 'GICS_SECT_PIT'
cfg_per_zs_ts = 12
cfg_per_weekly_adj = 1

# backtest
cfg_trade_q_map = {0:-1,9:1}
cfg_trade_sizing_vol = True


#******************************************************************
# exploring
#******************************************************************
def run_explore(df_model):
    df_data = df_model

    # bad idea?? ROE should be higher better
    #print(df_data[cfg_col_ihs_alpha+[cfg_col_y_rtn]].corr(method='spearman'))
    C = df_data[cfg_col_ihs_alpha].corr(method='spearman')
    print(scipy.linalg.eigh(C, eigvals_only=True))
    
    for iFld in cfg_col_alpha:
        df_data.plot.scatter(x=iFld,y=cfg_col_y_rtn)

    for iFld in cfg_col_alpha:
        sns.distplot(df_data[iFld].dropna())
        plt.figure()

    for iFld in cfg_col_alpha:
        sns.distplot(df_data[iFld].dropna())
        plt.figure()


#******************************************************************
# feature engineering
#******************************************************************

#-------------------------------
# helpers
#-------------------------------
# ? improve to just return series instead of manipulate df
def apply_qcut(dfg, cfg_fld_in, cfg_fld_out, cfg_tiles):
    if not dfg[cfg_fld_in].isnull().all() and not dfg[cfg_fld_in].min()==dfg[cfg_fld_in].max():
        idxSel=~dfg[cfg_fld_in].isnull()
        dfq = pd.cut(scipy.stats.rankdata(dfg.loc[idxSel,cfg_fld_in], 'ordinal'), cfg_tiles, labels=range(0, cfg_tiles), retbins=False)
        dfg.loc[idxSel,cfg_fld_out] = dfq
    else:
        dfg[cfg_fld_out] = pd.Series([np.nan]*dfg.shape[0])

# build factors for every period
def apply_alpha_stats_by_month(dfg):
    # single factor q
    for iFld in cfg_col_alpha:
        iFldRank = dfg[iFld].rank(ascending=iFld in cfg_col_alpha_high_better)
        dfg[iFld + '_rank'] = iFldRank
        apply_qcut(dfg,iFld + '_rank',iFld+'_q',cfg_tiles_features)

    # predictors peer comparison
    for iFld in cfg_col_alpha:
        dfg[iFld + '_zs_cur'] = (dfg[iFld]-np.nanmean(dfg[iFld]))/np.nanstd(dfg[iFld])
        #df[iFld + '_zs'] = df[iFld]-df[iFld].median()

    return dfg

def apply_roll_zscore(dfg, cfg_per):
    return dfg.rolling(window=cfg_per).apply(lambda x: ((x-np.nanmean(x))/np.nanstd(x))[-1])

def apply_alpha_model_zs_ts(dfg):
    for iFld in cfg_col_X:
        dfg[iFld+'_zs_ts'] = apply_roll_zscore(dfg[iFld+'_zs_cur'], cfg_per_zs_ts*cfg_per_weekly_adj)
#        dfg[iFld+'_ts'] = apply_roll_zscore(dfg[iFld], cfg_per_zs_ts*cfg_per_weekly_adj)

    return dfg


# helper generate features
def gen_features(df_model):

    df_model = df_model.groupby(['DATE_',cfg_fld_indu]).apply(apply_alpha_stats_by_month)
    #df_model = df_model.groupby('ID_SEC_TS').apply(apply_alpha_model_zs_ts)

    
    return df_model
    

#-------------------------------
# load data
#-------------------------------
if platform.python_version()[0]=='2':
    cfg_fname_sm_dtype_str = ['DATE_', 'DATE_YYYY_MM', 'DATE_YYYY_MM_MAX', 'DATE_IS_SUBMIT', 'ID_SEC',
           'ID_SEC_TS', 'ID_TICKER_PIT', 'ID_TICKER_CUR', 'ID_SEDOL_PIT',
           'ID_CUSIP_PIT', 'ID_SEDOL_CUR', 'ID_CUSIP_CUR', 'ID_RP_ENTITY',
           'GICS_SECT_PIT', 'GICS_INDU_PIT', 'GICS_SECT_PIT_ID',
           'GICS_INDU_PIT_ID', 'GICS_SUBIND_PIT_ID', 'RTN_FWD_1M_DATE']
    
    cfg_fname_sm_dtype = dict((key, str) for key in cfg_fname_sm_dtype_str)
    
    df_sm_csv=pd.read_csv(cfg_fname_sm_csv, dtype=cfg_fname_sm_dtype)
    df_sm_csv['DATE_']=pd.to_datetime(df_sm_csv['DATE_'], format='%Y-%m-%d')
    df_sm_csv['DATE_YYYY_MM_MAX']=pd.to_datetime(df_sm_csv['DATE_YYYY_MM_MAX'], format='%Y-%m-%d')
    df_sm_csv['RTN_FWD_1M_DATE']=pd.to_datetime(df_sm_csv['RTN_FWD_1M_DATE'], format='%Y-%m-%d')
    df_sm_csv['ID_SEC']=df_sm_csv['ID_SEC'].astype(int)
    df_sm_csv['DATE_IS_SUBMIT']=df_sm_csv['DATE_IS_SUBMIT'].astype(int)
    df_sm_raw = df_sm_csv

    df_rp_sentiment_csv = pd.read_csv(cfg_fname_rp_sentiment_csv)
    df_rp_sentiment = df_rp_sentiment_csv

else:
    df_sm_raw = pd.read_pickle(cfg_fname_sm)
    df_rp_sentiment = pd.read_pickle(cfg_fname_rp_sentiment)

df_ihs_model_rv = pd.read_csv(cfg_fname_ihs_model_rv)
df_ihs_factors = pd.read_csv(cfg_fname_ihs_factors)
df_qdl_sira = pd.read_csv(cfg_fname_qdl_sidr, dtype={'date':str})

#-------------------------------
# sec master
#-------------------------------
df_sm_raw = df_sm_raw.sort_values(['DATE_','ID_TICKER_PIT'])

def apply_vol(dfg):
    return dfg.ewm(alpha=(1.0-0.94)).std()*np.sqrt(255)

# remove duplicates
df_sm_raw = df_sm_raw.drop_duplicates(['DATE_','ID_SEC_TS'])

# vols
df_sm_raw['RTN_1D']=df_sm_raw.groupby('ID_TICKER_PIT')['PX_CLOSE_TR'].pct_change()
df_sm_raw['vol_ewm'] = df_sm_raw.groupby('ID_TICKER_PIT')['RTN_1D'].transform(apply_vol)

# filter submit data
df_sm = df_sm_raw[df_sm_raw['DATE_IS_SUBMIT']==1].copy()
df_sm = df_sm.drop(cfg_col_sm_cols_drop, axis=1)

# fill missing GICS
df_sm['GICS_SECT_PIT'] = df_sm[cfg_fld_indu].fillna(-1)

# industry relative returns
df_sm['RTN_FWD_1M_rel_zs'] = df_sm.groupby(['DATE_',cfg_fld_indu])['RTN_FWD_1M'].transform(lambda x: (x-np.nanmean(x))/np.nanstd(x))
df_sm['RTN_FWD_1M_rel'] = df_sm.groupby(['DATE_',cfg_fld_indu])['RTN_FWD_1M'].transform(lambda x: x-np.nanmedian(x))
df_sm['1M_PX_fwd_trade'] = df_sm.groupby('ID_TICKER_PIT')['PX_CLOSE_TR'].transform(lambda x: x.shift(-cfg_per_weekly_adj)/x-1)
df_sm['1M_PX_fwd_trade'] = df_sm['1M_PX_fwd_trade'].clip(-1.0, 2.0)


#-------------------------------
# ihs - model - rv
#-------------------------------
df_ihs_model_rv = df_ihs_model_rv.drop(cfg_col_ihs_cols_drop, axis=1)
df_ihs_model_rv['DATE_'] = pd.to_datetime(df_ihs_model_rv['DATE_'], format="%Y-%m-%d")

# add ihs
df_model = df_sm.merge(df_ihs_model_rv, on=['DATE_', 'ID_SEC_TS'], how='left')

# filter
df_model = df_model[~df_model['RTN_FWD_1M'].isnull()]
df_model = df_model[df_model[cfg_fld_indu]!=-1]
df_model = df_model[~df_model['GICS_SECT_PIT'].isin(['Financials','Health Care'])]

#-------------------------------
# ihs - model - factors
#-------------------------------
df_ihs_factors['DATE_'] = pd.to_datetime(df_ihs_factors['DATE_'], format="%Y-%m-%d")

df_ihs_factors=df_ihs_factors.merge(df_sm[['DATE_', 'ID_SEC_TS', 'GICS_SECT_PIT']], on=['DATE_', 'ID_SEC_TS'])

df_ihs_factors.head()

#t2=t[(t['DATE_']==t['DATE_'].max())&(t['GICS_SECT_PIT']=='Consumer Discretionary')]
#t2=t[(t['DATE_']==t['DATE_'].max())]#&(t['GICS_SECT_PIT']=='Consumer Discretionary')]

#t2[['fwdbp', 'fwdcp', 'fwdebitdaev', 'fwdsp', 'tw_ebitdaev',
#       'tw_sp', 'twbp', 'twcp']].hist()


df_ihs_factors[cfg_col_ihs_alpha_factors].isnull().sum()/df_ihs_factors.shape[0]


#-------------------------------
# ravenpack
#-------------------------------




#-------------------------------
# quandl
#-------------------------------
df_qdl_sidr = df_qdl_sira.rename(columns={'date':'DATE_YYYY_MM','ticker':'ID_TICKER_PIT'})
df_qdl_sidr['DATE_YYYY_MM'] = df_qdl_sidr['DATE_YYYY_MM'].str[0:4]+'-'+df_qdl_sidr['DATE_YYYY_MM'].str[-2:]


#-------------------------------
# run features
#-------------------------------

df_model = df_sm.merge(df_ihs_factors[['DATE_', 'ID_SEC_TS']+cfg_col_ihs_alpha_factors], on=['DATE_', 'ID_SEC_TS'], how='left')
df_model = df_model.merge(df_rp_sentiment.reset_index(),on=['DATE_YYYY_MM', 'ID_RP_ENTITY'], how='left')
df_model = df_model.merge(df_qdl_sidr[['DATE_YYYY_MM','ID_TICKER_PIT','sidr']],on=['DATE_YYYY_MM', 'ID_TICKER_PIT'], how='left')
df_model = gen_features(df_model)

print('*** loaded data features')

#******************************************************************
# run models
#******************************************************************

#-------------------------------
# helper fct
#-------------------------------

def apply_trading_signals1(dfg):
    for iFld in cfg_col_trade_q:
        sFldSig = 'trade_signal_'+iFld
        trade_signal=dfg[iFld].map(cfg_trade_q_map).fillna(0)
        dfg[sFldSig] = trade_signal
    return dfg

def apply_trading_signals2(dfg):
    for iFld in cfg_col_trade_q:
        sFld = 'trade_signal_'+iFld
        rollsum = dfg[sFld].rolling(12).sum().abs()
        dfg[iFld+'_rollsum']=rollsum
        dfg.loc[rollsum>=12,sFld]=0
    return dfg

def apply_port_sizing(dfg):
    for iFld in cfg_col_trade_q:
        sFldSig = 'trade_signal_'+iFld
        sFldPct = 'port_pct_'+iFld
        dfg[sFldPct] = 0.0
        idxLong = dfg[sFldSig]==1.0
        idxLongS = idxLong.sum()
        idxShort = dfg[sFldSig]==-1.0
        idxShortS = idxShort.sum()
        if(idxLongS>0 and idxShortS>0):
            if cfg_trade_sizing_vol:
                # longs
                dfg_long = dfg[idxLong]
                idxLongSvol = 1.0/np.sum(np.reciprocal(dfg_long['vol_ewm']))
                dfg.loc[idxLong,sFldPct] = idxLongSvol/dfg_long['vol_ewm']
                # shorts 
                dfg_short = dfg[idxShort]
                idxShortSvol = 1.0/np.sum(np.reciprocal(dfg_short['vol_ewm']))
                dfg.loc[idxShort,sFldPct] = -1.0*idxShortSvol/dfg_short['vol_ewm']
            else:
                dfg.loc[idxLong,sFldPct] = 1.0/idxLongS
                dfg.loc[idxShort,sFldPct] = -1.0/idxShortS

    return dfg

def apply_port_rtn(dfg):
    for iFld in cfg_col_trade_q:
        sFldPct = 'port_pct_'+iFld
        dfg['port_rtn_'+iFld]=dfg[sFldPct]*dfg['1M_PX_fwd_trade']

    dfg['port_pct_ew'] = 1.0/dfg.shape[0]
    dfg['trade_rtn_ew']=dfg['port_pct_ew']*dfg['1M_PX_fwd_trade']

    return dfg

cfg_lgbm_learn_init = 0.1
cfg_lgbm_param = {'boosting':'gbdt',
             'learning_rate':cfg_lgbm_learn_init,
             'num_threads':4,
             'device':'cpu',
             'max_depth': math.floor(np.sqrt(len(cfg_col_X))),
    'feature_fraction': 0.9,
    'verbose':0
             }
cfg_lgbm_num_round = 100

def run_model(df_bbg, cfg_dropna_X=True, cfg_dropna_y=True):
    #******************************************************************
    # walk-forward analysis
    #******************************************************************
    #cfg_dropna_X=True; cfg_dropna_y=True;

    cfg_lgbm_param['application'] = 'regression_l1'
    cfg_lgbm_param['metric']='l1'
    cfg_col_y_train = 'RTN_FWD_1M_rel_zs'
    cfg_col_pred = 'model_ols_reg_pred'
    
    model_ols = linear_model.LinearRegression()

    df_model[cfg_col_pred]=np.nan
    
    #iDateEnd=cfg_signals_dt_end[0]
    #iDateEnd=cfg_signals_dt_end[-1]
    for iDateEnd in cfg_signals_dt_end:
        
        print('trade', iDateEnd)

        # train data
        idx_df_train = (df_model['RTN_FWD_1M_DATE']<=iDateEnd)
        df_train = df_model[idx_df_train]

        if cfg_dropna_X:
            idx_train = ~(df_train[cfg_col_X_train].isnull().any(axis=1))
            #idy_train = ~(df_train[cfg_col_y_train].isnull().any(axis=1))
            #id_train = idx_train + idy_train 
            X_train = df_train.loc[idx_train, cfg_col_X_train]
            y_train = df_train.loc[idx_train, cfg_col_y_train]
        else:
            X_train = df_train[cfg_col_X_train]
            y_train = df_train[cfg_col_y_train]
            
            
        #y_train = y_train.fillna(0)
        print(np.sum(np.isnan(y_train)))
        y_train[np.isnan(y_train)]=0
        print(np.sum(np.isnan(y_train)))
        #X_train_lgb = lgb.Dataset(X_train, label=y_train)
        
        # live data
        idx_df_test = (df_model['DATE_']==iDateEnd)
        df_test = df_model[idx_df_test]
        X_test = df_test[cfg_col_X_train]
        if cfg_dropna_y:
            #idx_test_c = ~(df_test[cfg_col_X_train].isnull().all(axis=0))
            #X_test = X_test.drop(idx_test_c[idx_test_c==False].index,axis=1).head()
            #idx_test_r = ~(X_test.isnull().all(axis=1))
            #X_test = X_test[idx_test_r]
            X_test = X_test.fillna(0)
        else:
            X_test = df_test[cfg_col_X_train]

        # lgbm
        #model_lgbm = lgb.train(cfg_lgbm_param, X_train_lgb, cfg_lgbm_num_round)#, learning_rates=lambda iter: cfg_lgbm_learn_init * (0.99 ** iter))
        #y_test_pred = model_lgbm.predict(X_test, num_iteration=cfg_lgbm_num_round)        
        #print('lgbm pred',y_test_pred)
                
        #for iFld in cfg_col_X_train:
        #    sns.regplot(iFld,cfg_col_y_train, data=df_train,ci=None)
        #    plt.figure()
            
            #df=df.drop(['cola','colb'],axis=1)

        # OLS
        model_ols.fit(X_train, y_train.fillna(0))
        
        y_test_pred = model_ols.predict(X_test)
        print('ols pred', y_test_pred)
        
        # persist results
        # persist results
        if cfg_dropna_y:
            #df_model.loc[idx_df_test & idx_test_r, cfg_col_pred] = y_test_pred
            df_model.loc[idx_df_test , cfg_col_pred] = y_test_pred
        else:
            df_model.loc[idx_df_test, cfg_col_pred] = y_test_pred


    print('*** finish walk forward')
    
    return df_model


# dates
# np.set_printoptions(threshold=np.nan)
idx_model_sel = df_model['DATE_'].dt.year>=2013
cfg_signals_dt = df_model.loc[idx_model_sel,'DATE_'].unique()
cfg_signals_dt_end_offset = 0
cfg_signals_dt_end = cfg_signals_dt[cfg_signals_dt_end_offset::cfg_per_weekly_adj]

# check input
print(df_model.loc[idx_model_sel,cfg_col_X_train].isnull().sum()) 
df_model.loc[idx_model_sel,cfg_col_X_train+['DATE_']].groupby('DATE_').agg(lambda x: x.isnull().sum()).cumsum().plot() 

# start
df_model = run_model(df_model,cfg_dropna_X=True, cfg_dropna_y=True)
#df_model = run_model(df_model)

#df_model[cfg_col_pred].hist()

#-------------------------------
# trading signals
#-------------------------------
# model trading sianals
def apply_qcut_model(dfg):
    # single factor q
    for iFld in cfg_col_model_pred:
        apply_qcut(dfg,iFld,iFld+'_q',cfg_tiles_trade)
    return dfg
    
df_model = df_model.groupby(['DATE_',cfg_fld_indu]).apply(apply_qcut_model)

#******************************************************************
# run backtest
#******************************************************************
#-------------------------------
# portfolio sizing
#-------------------------------

cfg_col_sel = ['DATE_','ID_TICKER_PIT',cfg_fld_indu,'1M_PX_fwd_trade','vol_ewm'] + cfg_col_model_pred
df_pnl = df_model.loc[df_model['DATE_'].isin(cfg_signals_dt_end),cfg_col_sel+cfg_col_trade_q].copy()

df_pnl = df_pnl.groupby('DATE_').apply(apply_trading_signals1)
df_pnl = df_pnl.groupby('ID_TICKER_PIT').apply(apply_trading_signals2)
df_pnl = df_pnl.groupby('DATE_').apply(apply_port_sizing)
df_pnl = df_pnl.groupby('DATE_').apply(apply_port_rtn)



df_pnl.tail()


#-------------------------------
# backtest results
#-------------------------------

# equity curve
df_pnl.groupby('DATE_')[cfg_col_trade_rtn].sum().cumsum().plot(cmap='Set1')
df_pnl.groupby(['DATE_',cfg_fld_indu],as_index=False)['port_rtn_model_ols_reg_pred_q'].sum().pivot('DATE_',cfg_fld_indu,'port_rtn_model_ols_reg_pred_q').cumsum().plot(cmap='Set1')


# predicted vs actual return
df_pnl.plot.scatter('model_ols_reg_pred','1M_PX_fwd_trade')
print('IC',df_pnl[['model_ols_reg_pred','1M_PX_fwd_trade']].corr(method='spearman'))

# q-tile median return
dfg1=df_pnl.groupby('model_ols_reg_pred_q')['1M_PX_fwd_trade'].median()*12*cfg_per_weekly_adj
dfg2=df_pnl.groupby('rvm_ttmfcfp_q')['1M_PX_fwd_trade'].median()*12*cfg_per_weekly_adj
pd.concat([dfg1,dfg2],axis=1).plot.bar(title='rtn x-tiles')

dfg1=df_pnl.groupby('model_ols_reg_pred_q')['vol_ewm'].median()
dfg2=df_pnl.groupby('rvm_ttmfcfp_q')['vol_ewm'].median()
pd.concat([dfg1,dfg2],axis=1).plot.bar(title='vol x-tiles')

#******************************************************************
# submit to factset
#******************************************************************
cfg_col_submit1 = ['DATE_', 'ID_SEDOL_PIT','model_ols_reg_pred'] 
cfg_col_submit2 = ['Portfolio Name', 'ID_SEDOL_PIT', 'DATE_', 'Alpha']
cfg_col_submit2_rename = {'ID_SEDOL_PIT':'Symbol', 'DATE_':'Date'}
df_submit = df_model.loc[idx_model_sel,cfg_col_submit1].copy()

#!!! make sure no outdate alpha estimates
df_submit['model_ols_reg_pred']=df_submit['model_ols_reg_pred'].fillna(0)
df_submit['Alpha']=df_submit.groupby('DATE_')['model_ols_reg_pred'].transform(lambda x: (x-np.nanmean(x))/np.nanstd(x))

print('% missing SEDOL',df_submit['ID_SEDOL_PIT'].isnull().sum()/df_submit.shape[0])

df_submit['Portfolio Name']='ALPHA_HACK_TEAM4'

df_submit[cfg_col_submit2].rename(columns=cfg_col_submit2_rename).dropna().to_csv('data-csv/ALPHA_HACK_TEAM4.csv',index=False)
